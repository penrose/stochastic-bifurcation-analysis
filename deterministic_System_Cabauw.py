#!/usr/bin/env python
# coding: utf-8

# # **Bifurcation Analysis of a Temperature Inversion Strength Model**

# 
# $\frac{\Delta T}{dt} = Q_i - \lambda \Delta T - \rho c_p \left( \frac{k}{\log\frac{z_r}{z_0}}\right)^2 U \Delta T f(\Delta T)$
# 
# The model for temperature inversion strength will be analysed for three different stability functions $f$.
# 
# In the following, the temperature inversion strength $\Delta T$ is denoted as $x$.

# In[1]:



import PyDSTool as dst
import numpy as np
from matplotlib import pyplot as plt

get_ipython().run_line_magic('matplotlib', 'inline')


# ### System specification

# In[2]:


# name of the model
'''‘args’ class imported from PyDSTool, essentially a structure that collects together data attributes in an 
addressable way. use it here to build all the information needed to create a valid dynamical system mode'''
DSargs = dst.args(name='temperature inversion strength model') 

# parameters of Data from Cabauw
DSargs.pars = { 'q': 70,        # isothermal net radiation
                'l': 4,         # lumped parameter
                'U': 8.9,      # wind speed
                'rho': 1.2,     # air density
                'c_p': 1005,    # heat capacity of air
                'k': 0.4,       # Kármán constant
                'g': 9.81,      # gravitational constant
                'T_r': 285,     # temparature in reference height
                'z_r': 40,      # reference height
                'z_0': 0.03,    # temperature measure point above surface
                'alpha': 5}     

'''Stability functions: 
cut_off: 'if(x < pow(U,2) * T_r / (2 * alpha * z_r * g), 1 - 2 * alpha * z_r * g/T_r * x/pow(U,2), 0)'
long-tail: 'exp(-2 * alpha * z_r * g/T_r * x/pow(U,2))'
short-tail: 'exp(-2 * alpha * z_r * g/T_r * x/pow(U,2) - pow((alpha * z_r * g/T_r * x/pow(U,2)),2))'
'''
DSargs.fnspecs  = {'f': (['x'], 'exp(-2 * alpha * z_r * g/T_r * x/pow(U,2) - pow((alpha * z_r * g/T_r * x/pow(U,2)),2))') } 
# rhs of the differential equation, including dummy variable w
DSargs.varspecs = {'x': 'q - l*x - rho * c_p * pow((k/log(z_r/z_0)),2) * U * x * f(x)',   
                   'w': 'x-w' }
# initial conditions
DSargs.ics      = {'x': 5, 'w': 5}


# 'w' is a dummy variable which is necessary to permit the 2-parameter continuation of the limit points later on which otherwise cannot be done for a 1D dynamical system. It only tracks x.

# ### **Integral curves**
# Computaion of the trajectories of the differential equation by using a Generator instance.

# In[3]:


DSargs.tdomain = [0,3]                          # set the range of integration.
ode  = dst.Generator.Vode_ODEsystem(DSargs)     # an instance of the 'Generator' class.
traj = ode.compute('inversion')                 # integrate ODE
pts  = traj.sample(dt=0.1)                      # Data for plotting

# PyPlot commands
plt.plot(pts['t'], pts['x'])
plt.xlabel('time')                              # Axes labels
plt.ylabel('temperature')                       # ...
plt.ylim([0,40])                                # Range of the y axis
plt.title(ode.name)                             # Figure title from model name
plt.show()


# In[4]:


fig = plt.figure(figsize=(7,5))
plt.clf()                                        # Clear the figure
#plt.hold(True)                                  # Sequences of plot commands will not clear the existing figure, 
                                                 # depends on python-version whether this command is necessary
for i, x0 in enumerate(np.linspace(0,25,26)):    # interval of initial temperature values and number of solution curves
    ode.set( ics = { 'x': x0 } )                # Initial condition
    # Trajectories are called inv0, inv1, ...
    # sample them on the fly to create Pointset tmp
    tmp = ode.compute('pol%3i' % i).sample()    # or specify dt option to sample to sub-sample
    plt.plot(tmp['t'], tmp['x'])
plt.xlabel('$t$ in $s$', fontsize = 24)
plt.ylabel('$\Delta T$ in $K$', fontsize = 24)
plt.title('')
plt.tick_params(axis='both', which='major', labelsize=18)
plt.locator_params(axis="x", nbins=8)
plt.locator_params(axis="y", nbins=8)
plt.gcf().subplots_adjust(left=0.2, bottom =0.2)
plt.savefig('multiple_ics.png')
plt.show()


# Several solution curves of varying initial temperature differences. For a small lumped parameter $\lambda$ = 3 Wm⁻²K⁻¹ and a wind speed U = 9 ms⁻¹, three equilibria can be observed: Two stable equilibria, towards which solutions with close initial conditions move over time and, between them, one unstable equilibrium where solutions quickly move away over time.

# ### **Bifurcation Diagram**

# In[5]:


# Prepare the system to start close to a steady state
ode.set(pars = {'U': 0.01} )           # Lower bound of the control parameter 'U'
ode.set(ics =  {'x': 1.3} )       # Close to one of the steady states present for i=-220

PC = dst.ContClass(ode)               # Set up continuation class

PCargs = dst.args(name='EQ1', type='EP-C')     # 'EP-C' stands for Equilibrium Point Curve. The branch will be labeled 'EQ1'.
PCargs.freepars     = ['U']                    # control parameter(s) (it should be among those specified in DSargs.pars)
PCargs.MaxNumPoints = 64                       # The following 3 parameters are set after trial-and-error
PCargs.MaxStepSize  = 1
PCargs.MinStepSize  = 1e-5
PCargs.StepSize     = 2e-2
PCargs.LocBifPoints = 'LP'                     # detect limit points / saddle-node bifurcations
PCargs.SaveEigen    = True                     # to tell unstable from stable branches


# In[6]:


PC.newCurve(PCargs)
PC['EQ1'].forward()
PC.display(['U','x'], stability=True, figure=3)        # stable and unstable branches as solid and dashed curves, resp.
plt.title('')
plt.tick_params(axis='both', which='major', labelsize=18)
plt.locator_params(axis="x", nbins=8)
plt.locator_params(axis="y", nbins=8)
plt.xlabel('U in $ms^{-1}$', fontsize = 24)
plt.ylabel('$\Delta$T in K', fontsize = 24)
plt.gcf().subplots_adjust(left=0.2, bottom =0.2)
#plt.savefig('bifurcation_diagram_shorttail_carbauw_l7_z03.png')


# Typical S-shape of equilibrium solution branch, appears for all three stability functions. 
# Histeresis-effect for small lumped parameter values: single stable equilibriumm for U < LP2 and single stable equilibrium for U >  LP2. For LP2 < U < LP1 two stable and one unstable equilibrium with a fast changing position.

# ### Information of the equilibrium curve:

# In[7]:


PC['EQ1'].info()


# ### Information about the bifurcation points LP1 and LP2:

# In[8]:


print(PC['EQ1'].getSpecialPoint('LP1')) 


# In[9]:


print(PC['EQ1'].getSpecialPoint('LP2')) 


# In[10]:


print(PC['EQ1'].getSpecialPoint('P1')) 


# In[11]:


print(PC['EQ1'].getSpecialPoint('P2')) 


# ### Change of the bifurcation points, when $\lambda$ varies:

# In[ ]:


PCargs = dst.args(name='SN1', type='LP-C')
PCargs.initpoint    = 'EQ1:LP1'
PCargs.freepars     = ['U', 'l']
PCargs.MaxStepSize  = 2
PCargs.LocBifPoints = ['CP']
PCargs.MaxNumPoints = 200
PC.newCurve(PCargs)
PC['SN1'].forward()
PC['SN1'].backward()
PC['SN1'].display(['U','l'], figure=4)


# In[ ]:


PCargs = dst.args(name='SN2', type='LP-C')
PCargs.initpoint    = 'EQ1:LP2'
PCargs.freepars     = ['U', 'l']
PCargs.MaxStepSize  = 2
PCargs.LocBifPoints = ['CP']
PCargs.MaxNumPoints = 200
PC.newCurve(PCargs)
PC['SN2'].forward()
PC['SN2'].backward()
PC['SN2'].display(['U','l'], figure=4)


# In[ ]:




